# WEB CRAWLING OF CITY ARTICLES USING SCRAPY.SPIDER

import scrapy               # for Scraping
from scrapy import Request  # for multiple page Scraping
import csv                  # for read and write csv file
import os                   # for making directory
from scrapy.crawler import CrawlerProcess 


# Reads and Stores Contents from AVA_Intelligence DB as csv
class Read:
    
    Country_Info={}
    Country_List =[]
    City_List=[]

    with open(r'C:\Users\sienr\brickset-scraper\Retrip\Retrip_Scrap\CSV_info\Master.csv',encoding='utf-8') as f:
        r = csv.reader(f,delimiter=',')
        for i , row in enumerate(r) :
            if i > 0 :
                
                # Stores Region,Country and City as List
                Country_List.append(row[0:3])

                # Stores Cities
                City_List.append(row[2])

                # Stores City and City_ID as Dictionary
                Country_Info[ row[2] ] = row[3] 


# Scrapping Class
class Scrap(scrapy.Spider):
    
    name = "Scrap"
    start_urls = ['https://rtrp.jp/locations/'] 
    download_delay = 1.5                        # Delay for each request
    Visit = {}                                  # Check whether the site has already been visited
    Page = {}                                   # Updates page_number of each link 
    
    # Gets Countries and URLs matched in Master.csv
    def parse(self,response) :

        Contents ={}
        Country_Set = '.areaBlock '
        for Country in response.css(Country_Set) :

            Title_Set = 'a::attr(title)'
            Link_Set = 'a::attr(href)'

            Title = Country.css(Title_Set).extract_first()
            Link = Country.css(Link_Set).extract_first()

            Contents[Title]=Link

        for item in Contents:
        
            # Pass country found in Master.csv 
            if any(item in els for els in Read.Country_List) :
                
                # Makes a directory in PC
                Path = r'C:\Users\sienr\brickset-scraper\Retrip\Retrip_Scrap\CSV_info\\'+item
                try :
                    os.mkdir(Path)
                except OSError:
                    pass

                # Makes a Request for each Country URLs
                yield Request(response.urljoin(str(Contents[item])),callback=self.parse_country)

    # Gets Info for each Country
    def parse_country(self,response):
    
        Country = response.css('.breadList li:nth-child(4) ::text').extract_first()
        List = response.css('.subCategories.hot_locations a.subCategoryName')
        
        # Checks if visited site has city subcategories
        if List:

            for city in List:

                
                City_Name = city.css('a ::text').extract_first().strip()
                
                # Pass City found in Master.csv
                if any( (str(elm) in City_Name or City_Name in str(elm) ) for elm in Read.City_List):
                    for els in Read.City_List:
                        if str(els) in City_Name or City_Name in str(els):
                            AVA_ID = Read.Country_Info[els]
                            break
                else:
                    continue
        
                with open(r'C:\Users\sienr\brickset-scraper\Retrip\Retrip_Scrap\CSV_info\\'+Country+'\\'+City_Name+'.csv','w',encoding='utf-16') as f:
                    w = csv.writer(f,lineterminator='\n')
                    w.writerow(['Country','City','City_Id','AVA_ID','URL'])
                    
                    City_Link = city.css('a ::attr(href)').extract_first()
                    City_id = City_Link.strip('https://rtrp.jp/locations/')
                    Article_Link = City_Link + 'articles/'

                    w.writerow([Country,City_Name,City_id,AVA_ID,City_Link])

                # Initial Page of the link
                self.Page[City_Name] = 1

                # Makes Request for each City URLs
                yield Request(response.urljoin(str(Article_Link)),callback=self.parse_city)

        # If the visited link is City
        else :

            City_Name = response.css('.contentTopTitle h1::text').extract_first().strip()
            
            if any( (str(elm) in City_Name or City_Name in str(elm) ) for elm in Read.City_List) :
                pass
            else:
                return

            City_Link = response.request.url
            City_id = City_Link.strip('https://rtrp.jp/locations/')
            Article_Link = City_Link + 'articles/'

            with open(r'C:\Users\sienr\brickset-scraper\Retrip\Retrip_Scrap\CSV_info\\'+Country+'\\'+City_Name+'.csv','w',encoding='utf-16') as f:
                w = csv.writer(f,lineterminator='\n')
                w.writerow(['Country','City','City_Id','AVA_ID','URL'])
                
                for els in Read.City_List:       
                    if str(els) in City_Name or City_Name in str(els):
                        AVA_ID = Read.Country_Info[els]
                        w.writerow([Country,City_Name,City_id,AVA_ID,City_Link])
                        break

            self.Page[City_Name] = 1

            yield Request(response.urljoin(str(Article_Link)),callback=self.parse_city)

    # Gets Article Contents
    def parse_city(self,response):
        

        Country = response.css('.breadList li:nth-child(4) ::text').extract_first()
        City_Name = response.css('.contentTopTitle h1::text').extract_first().strip()
        Current_Page = str(response.request.url).strip()

        # Checks whether current page has been visited
        if Current_Page in self.Visit and self.Visit[Current_Page]=='CHECK' :
            print(Country,City_Name,'DONE')
            return                              # returns if the Site has been visited
        else :
            self.Visit[Current_Page] = 'CHECK'  # mark as visited   
            Page_Num = self.Page[City_Name]     # gets the ongoing page number the city
     
        with open(r'C:\Users\sienr\brickset-scraper\Retrip\Retrip_Scrap\CSV_info\\'+Country+'\\'+City_Name+'.csv','a',encoding='utf-16') as f:
            w = csv.writer(f,lineterminator='\n')
            if Page_Num == 1 :
                w.writerow(['Title','Explain','Date','View','Fav','Tag','Writer','Aricle_Link'])
            
            Article_Selector = '.exp '
            for article in response.css(Article_Selector):

                Title_Selector = 'a ::attr(title)'
                Exp_Selector = 'p ::text'
                Date_Selector = '.sub ::text'
                Tag_Selector ='.icon-tag ~ a::text'
                Views_Selector = '.countView ::text'
                Favs_Selector = '.favView ::text'
                Article_Ref_Name_Selector = '.name ::text'
                Article_Link_Selector = ' .title ::attr(href)'
                    
                Title = article.css(Title_Selector).extract_first()
                Explanation = article.css(Exp_Selector).extract_first()
                Date = article.css(Date_Selector).extract_first()
                Tags = article.css(Tag_Selector).extract()
                Views = article.css(Views_Selector).extract_first()
                Favs = article.css(Favs_Selector).extract_first()
                Reference = article.css(Article_Ref_Name_Selector).extract_first()
                URL = article.css(Article_Link_Selector).extract_first()

                w.writerow([Title,Explanation,Date,Views,Favs,Tags,Reference,URL])

            
            # try if the page has next page
            try:
                Next = response.css('a.next').extract_first()
            
            except:
                pass
            else:
                
                # Makes request to go to the next page
                if Next:
                
                    Current_Page= Current_Page.strip('?page=' + str(Page_Num))
                    Next_Page = '?page=' + str(Page_Num+1)
                    self.Page[City_Name]+=1
                    
                    yield Request(
                            
                        str(Current_Page+Next_Page), 
                        callback=self.parse_city,
                        dont_filter = True
                    )

def main():

    process = CrawlerProcess()
    process.crawl(Scrap)
    process.start()
    process.stop()
    print('ALL DONE')

if __name__ == '__main__' :

    main()