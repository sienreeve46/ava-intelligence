import scrapy
import csv


class Scrap(scrapy.Spider):
    
    name= "Retripscrap"
    start_urls = ['https://retrip.jp/']

    f = csv.writer(open('RetripScraper.csv', 'w'))
    f.writerow(['Title'])#, 'Explanation' , 'Location','Date', 'Views','Reference','Image Link','Web Link'])
    
    def parse(self,response):


        Article_Selector = '.expBottomGridList .exp '
        f = csv.writer(open('RetripScraper.csv', 'w',newline=''))
        for article in response.css(Article_Selector):

            Title_Selector = 'a ::text'
            Exp_Selector = 'p ::text'
            Location_Selector = 'a[href^="/locations/"] ::text'
            Date_Selector = '.sub ::text'
            Views_Selector = '.countView ::text'
            Article_Ref_Name_Selector = '.name ::attr(href)'
            Image_Selector = ' ::attr(style)'
            Article_Link_Selector = ' .title ::attr(href)'
                
            Title = article.css(Title_Selector).extract_first()
            Explanation = article.css(Exp_Selector).extract_first()
            Location = article.css(Location_Selector).extract_first()
            Date = article.css(Date_Selector).extract_first()
            Views = article.css(Views_Selector).extract_first()
            Reference = article.css(Article_Ref_Name_Selector).extract_first()
            Image = article.css(Image_Selector).extract_first()
            URL = article.css(Article_Link_Selector).extract_first()
             
        
            
            f.writerow([Title])#Explanation , Location, Date, Views, Reference, Image, URL]) 

            yield{
                
                'Title': Title,
                'Explain' : Explanation,
                'Location' : Location,
                'Date' : Date,
                'Views' : Views,
                'Article_Ref' : Reference,
                'Image' : Image,
                'Link' : URL
            }

            

#        Next_Page_Selector = '[rel=next] ::attr(href)'
#       next_page = response.css(Next_Page_Selector).extract_first()
#        if next_page :

 #           yield scrapy.Request(
                
 #               response.urljoin(next_page), 
  #              callback=self.parse
  #          )
                