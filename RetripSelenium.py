# Requred packages
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import csv

f = csv.writer(open('retripselenium.csv','w'))
f.writerow(['Title','Description','Date','Location','Link'])

chrome_options = Options()
chrome_options.add_argument("--incognito")
chrome_options.add_argument("--window-size=1920x1080")
chrome_options.add_argument('--lang=ja')

path=(r"C:\Users\sienr\brickset-scraper\chromedriver.exe")

driver = webdriver.Chrome(chrome_options=chrome_options,executable_path=path)

url = "https://retrip.jp/"
driver.get(url)
time.sleep(2)


article_titles = driver.find_elements_by_css_selector(".articleMain .title a")

Titles = [el.text for el in article_titles]
Titles_Link = [el.get_attribute("href") for el in article_titles]

article_Description = driver.find_elements_by_css_selector(".articleMain p")
Description = [el.text for el in article_Description]

article_Location = driver.find_elements_by_css_selector(".articleMain .locationBl a")
Location = [el.text for el in article_Location]

article_Date = driver.find_elements_by_css_selector(".articleMain .sub")
Date =[ el.text for el in article_Date]

for item in range(len(Titles)):

    f.writerow([Titles[item],Description[item],Date[item],Location[item],Titles_Link[item]])

f.close()
print("DONE")

